libtest-html-w3c-perl (0.04-2) UNRELEASED; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team. Closes: #924996
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Change bugtracker URL(s) to HTTPS.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Set Testsuite header for perl package.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 06 Jan 2013 22:07:42 +0100

libtest-html-w3c-perl (0.04-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Thu, 07 Jan 2021 13:24:12 +0100

libtest-html-w3c-perl (0.04-1) unstable; urgency=low

  [ Harlan Lieberman-Berg ]
  * Team upload
  * New upstream version (0.04).
  * Bump compat, debhelper to 8, s-v to 3.9.2 without changes.
  * Remove replace_boilerplate_text patch, as it has been applied
    upstream.
  * Add new DEP-5 Format, replace headers with compliant versions.
  * Bump upstream copyright year.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Harlan Lieberman-Berg <H.LiebermanBerg@gmail.com>  Mon, 22 Aug 2011 12:38:22 -0400

libtest-html-w3c-perl (0.03-2) unstable; urgency=low

  * Handle case where no sensible output returned from validator.
    Previously this caused the 'diag_html' method to crash. (Closes: #618335)

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Mon, 14 Mar 2011 13:34:09 +0000

libtest-html-w3c-perl (0.03-1) unstable; urgency=low

  * Initial Release. (Closes: #608824)

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Tue, 04 Jan 2011 18:52:15 +0000
